
package org.onebeartoe.paging.mission.control;

import java.util.List;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;
import org.testng.annotations.Test;

/**
 * This class verifies the specification for the 
 * TelemetryService class.
 */
public class TelemetryServiceSpecification
{
    private final TelemetryService implementation;
    
    public TelemetryServiceSpecification()
    {
        implementation = new TelemetryService();
    }

    /**
     * This verifies the input file is present at runtime and that it has
     * the expected number of records.
     */
    @Test
    public void nextDataset() throws Exception 
    {
        List<Telemetry> telemetry = implementation.nextDataset();
        
        assertNotNull(telemetry);
        
        int actual = telemetry.size();
        
        int expected = 14;
        
        assertEquals(expected, actual);
    }
}
