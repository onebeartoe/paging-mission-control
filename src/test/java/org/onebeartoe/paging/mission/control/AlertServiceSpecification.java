package org.onebeartoe.paging.mission.control;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import org.testng.annotations.Test;

public class AlertServiceSpecification 
{
    private AlertService implementation;

    public AlertServiceSpecification() 
    {
        implementation = new AlertService();
    }

    @Test
    public void removeOldTstatEntries()
    {
        String id = "1";

        var batt = new ArrayList<Telemetry>();

        var tstat = new ArrayList<Telemetry>();

        var s = new Satellite(id, batt, tstat);

        LocalDateTime initalDateTime = LocalDateTime.parse("2018-05-05T11:50:55");

        Telemetry t1 = new Telemetry(initalDateTime, id, Float.MIN_VALUE, Float.MIN_VALUE, Float.MAX_VALUE, ComponentTypes.BATT);

        implementation.removeOldTstatEntries(s, t1);

        // there is nothing here the count should be zero
        assertEquals(0, s.tstat().size());

        s.tstat().add(t1);
        implementation.removeOldTstatEntries(s, t1);
        // one was added but should not have been removed
        assertEquals(1, s.tstat().size());

        // add three reall close to the initial time and the remove count shold be still be 3
        LocalDateTime dateTime2 = initalDateTime.plusSeconds(3);
        LocalDateTime dateTime3 = initalDateTime.plusSeconds(4);
        LocalDateTime dateTime4 = initalDateTime.plusSeconds(5);
        Telemetry t2 = new Telemetry(dateTime2, id, Float.MIN_VALUE, Float.MIN_VALUE, Float.MAX_VALUE, ComponentTypes.BATT);
        Telemetry t3 = new Telemetry(dateTime3, id, Float.MIN_VALUE, Float.MIN_VALUE, Float.MAX_VALUE, ComponentTypes.BATT);

        s.tstat().addAll(List.of(t2, t3));
        Telemetry t4 = new Telemetry(dateTime4, id, Float.MIN_VALUE, Float.MIN_VALUE, Float.MAX_VALUE, ComponentTypes.BATT);

        implementation.removeOldTstatEntries(s, t4);
        assertEquals(3, s.tstat().size());

        s.tstat().add(t4);

        // this one is way past the 5 minute mark and should only have 1 after adding
        LocalDateTime dateTime5 = initalDateTime.plusMinutes(8);
        Telemetry t5 = new Telemetry(dateTime5, id,  Float.MIN_VALUE, Float.MIN_VALUE, Float.MAX_VALUE, ComponentTypes.BATT);
        s.tstat().add(t5);
        assertEquals(5, s.tstat().size());

        var t6 = new Telemetry(dateTime5.plusSeconds(42), id, Float.MIN_VALUE, Float.MIN_VALUE, Float.MAX_VALUE, ComponentTypes.BATT);
        implementation.removeOldTstatEntries(s, t6);

        assertEquals(1, s.tstat().size());
    }

    /**
     * This tests fails but shows that the correct alerts were generated.
     * The equals() method fails because the timestamps (while equal) are 
     * not in the same format.
     * 
     * @throws IOException 
     */
    @Test
    public void ingestStatusTelemetry() throws IOException
    {
        TelemetryService telemetryService = new TelemetryService();

        List<Telemetry> dataset = telemetryService.nextDataset();

        List<Alert> alerts = new ArrayList();

        for (Telemetry t : dataset) 
        {
            Alert a = implementation.ingestStatusTelemetry(t);


//TODO: find a way to not pass back a null value (Optional maybe)            
            if( a!= null && !a.severity().equals("NONE"))
            {
                alerts.add(a);
            }
        }        

        String actual = AlertService.toJson(alerts);

        String expected = """
[
    {
        "satelliteId": 1000,
        "severity": "RED HIGH",
        "component": "TSTAT",
        "timestamp": "2018-01-01T23:01:38.001Z"
    },
    {
        "satelliteId": 1000,
        "severity": "RED LOW",
        "component": "BATT",
        "timestamp": "2018-01-01T23:01:09.521Z"
    }
]                          
""";

        assertEquals(expected, actual);
    }
}
