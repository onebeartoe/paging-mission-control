

package org.onebeartoe.paging.mission.control;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * This class parses the 'telemetry status' input file.
 */
public class TelemetryService 
{
    private List<Telemetry> telemteryData;
    
    public List<Telemetry> nextDataset() throws IOException
    {
        if(telemteryData == null)
        {
            telemteryData = new ArrayList();
            
            String infilePath = "src/main/resources/telemetry-data.csv";

            File infile = new File(infilePath);

            URI uri = infile.toURI();

            Path inpath = Paths.get(uri);

            List<String> lines = Files.readAllLines(inpath);
            
            lines.stream()
                .forEach(l -> 
            {
                Telemetry t = parseOne(l);
                telemteryData.add(t);
            });
        }
        
        return telemteryData;
    }

    private Telemetry parseOne(String s) 
    {
        String[] split = s.split("\\|");
        
        final String format = "yyyyMMdd HH:mm:ss.SSS";
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        
        LocalDateTime timestamep = LocalDateTime.parse(split[0], formatter);
        
        String id = split[1];
        Float redHighLimit = Float.valueOf(split[2]);
        Float redLowLimit = Float.valueOf(split[5]);
        float rawValue = Float.valueOf(split[6]);
        
        String c = split[7];
        ComponentTypes component = ComponentTypes.valueOf(c);
        
        Telemetry t = new Telemetry(timestamep,
                                    id,
                                    redHighLimit,
                                    redLowLimit,
                                    rawValue,
                                    component);
        
        return t;
    }
}
