
package org.onebeartoe.paging.mission.control;

import java.time.LocalDateTime;

/**
 *
 */
public record Alert(String id,
                    String severity,
                    String component,
                    LocalDateTime timestamep)
{

}
