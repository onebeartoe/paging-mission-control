
package org.onebeartoe.paging.mission.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides an entry point into exercising the AlertService class 
 * from the command line.
 */
public class MissionControl 
{
    public static void main(String[] args) throws IOException 
    {
        TelemetryService telemetryService = new TelemetryService();

        List<Telemetry> dataset = telemetryService.nextDataset();

        AlertService alertService = new AlertService();

        List<Alert> alerts = new ArrayList();
        
        for (Telemetry t : dataset) 
        {
            Alert a = alertService.ingestStatusTelemetry(t);

//TODO: find a way to not pass back a null value (Optional maybe)            
            if( a!= null && !a.severity().equals("NONE"))
            {
                alerts.add (a);
            }
        }

        var json = AlertService.toJson(alerts);
        
        System.out.println(json);        
    }
}
