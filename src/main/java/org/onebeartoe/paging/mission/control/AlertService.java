
package org.onebeartoe.paging.mission.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class has business logic for determining if an alert should be raised.
 * 
 */
public class AlertService 
{
    private Map<String, Satellite> idToSatelliteMap;
    
    public AlertService()
    {
        idToSatelliteMap = new HashMap();
    }
    
    /**
     * This method takes in telemetry data in the form one entry per string in 
     * the List.
     * 
     * @param inputFile
     * @return a JSON representation of any telemetry status warning
     */
    public Alert ingestStatusTelemetry(Telemetry latestTelemetry) throws JsonProcessingException 
    {        
        String targetId = latestTelemetry.id();
        
        Satellite s = idToSatelliteMap.get(targetId); 
        
        if(s == null)
        {
            List<Telemetry> batt = new ArrayList();
            List<Telemetry> tstat = new ArrayList();
            s = new Satellite(targetId, batt, tstat);
            
            // this ID has not been seen 
            idToSatelliteMap.put(targetId, s);
        }

        AlertTypes alertType = analyzeForAlert(latestTelemetry);

        Alert alert;
        
        if(alertType == AlertTypes.NONE)
        {
            alert = new Alert(targetId, "NONE", latestTelemetry.component().name(), latestTelemetry.timestamep());
        }
        else
        {
            alert = processAlert(s, latestTelemetry, alertType);
        } 

        return alert;
    }
    
    private Alert processAlert(Satellite s, Telemetry latestTelemetry, AlertTypes alertType) throws JsonProcessingException
    {
//TODO: find a way to not pass back a null value (Optional maybe)        
        Alert alert = null;
        
        if(latestTelemetry.component() == ComponentTypes.BATT)
        {
            removeOldBattEntries(s, latestTelemetry);

            s.batt().add(latestTelemetry);

            if( s.batt().size() >= 3)
            {
                Telemetry firstTelemetry = s.batt().get(0);

                alert = new Alert(firstTelemetry.id(),
                                        alertType.name().replace("_", " "),
                                        latestTelemetry.component().name(),
                                        latestTelemetry.timestamep());
            }
        }
        else if(latestTelemetry.component() == ComponentTypes.TSTAT)
        {
            removeOldTstatEntries(s, latestTelemetry);

            s.tstat().add(latestTelemetry);

            if(s.tstat().size() >= 3)
            {
                Telemetry firstTelemetry = s.tstat().get(0);

                alert = new Alert(firstTelemetry.id(),
                        alertType.name().replace("_", " "),
                        latestTelemetry.component().name(),
                        latestTelemetry.timestamep());
            }
        }        

        return alert;
    }

    private AlertTypes analyzeForAlert(Telemetry latestTelemetry) 
    {
        AlertTypes type = AlertTypes.NONE;
        
        if(latestTelemetry.rawValue() >= latestTelemetry.redHighLimit())
        {
            type = AlertTypes.RED_HIGH;
        }
        else 
            if(latestTelemetry.rawValue() <= latestTelemetry.redLowLimit())
        {
            type = AlertTypes.RED_LOW;
        }
        
        return type;
    }

//TODO: refactor the next two method bodies into one.    
    private void removeOldBattEntries(Satellite s, Telemetry latestTelemetry)
    {
        LocalDateTime expirationPoint = latestTelemetry.timestamep().minus(Duration.ofMinutes(5) );
        
        List<Telemetry> gonnaRemove = s.batt()
                                       .stream()
                                       .filter( p -> p.timestamep().isBefore(expirationPoint))
                                       .collect( Collectors.toList() );
        
        s.batt().removeAll(gonnaRemove);
    }

    public void removeOldTstatEntries(Satellite s, Telemetry latestTelemetry) 
    {
        LocalDateTime expirationPoint = latestTelemetry.timestamep().minus(Duration.ofMinutes(5) );
        
        List<Telemetry> gonnaRemove = s.tstat()
                                       .stream()
                                       .filter( p -> p.timestamep().isBefore(expirationPoint))
                                       .collect( Collectors.toList() );
        
        s.tstat().removeAll(gonnaRemove);
    }
    
    public static String toJson(List<Alert> alerts) throws JsonProcessingException
    {
        var mapper = new ObjectMapper();
                        
        mapper.enable(SerializationFeature.INDENT_OUTPUT);                        

        mapper.registerModule(new JavaTimeModule());

        return mapper.writeValueAsString(alerts);  
    }    
}
