
package org.onebeartoe.paging.mission.control;

/**
 * This enum represents the differenty types of alerts, including none.
 */
public enum AlertTypes 
{
    NONE,
    RED_HIGH,
    RED_LOW;
}
