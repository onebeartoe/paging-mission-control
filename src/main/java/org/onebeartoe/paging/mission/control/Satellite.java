
package org.onebeartoe.paging.mission.control;

import java.util.List;

/**
 * 
 */
public record Satellite(String id, List<Telemetry> batt, List<Telemetry> tstat)
{
    
}
