
package org.onebeartoe.paging.mission.control;

import java.time.LocalDateTime;

/**
 * This record type encapsulates telemetry data for a satellite's sensors.
 */
public record Telemetry(LocalDateTime timestamep,
        String id,
        Float redHighLimit,
        Float redLowLimit,
        Float rawValue,
        ComponentTypes component) 
{

}
