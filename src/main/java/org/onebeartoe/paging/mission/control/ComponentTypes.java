
package org.onebeartoe.paging.mission.control;

/**
 * This type enumerates the different satellite's sensors.
 */
public enum ComponentTypes 
{
    BATT,
    TSTAT;
}
